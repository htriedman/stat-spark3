from pyspark.sql import functions as sf
import math

def calc_abs_err(df, exact_col='count', noisy_col='gbc'):
    return df.withColumn('abs_err', sf.abs(sf.col(exact_col) - sf.col(noisy_col)))

def calc_rel_err(df, exact_col='count', noisy_col='gbc'):
    '''
    Calculates relative error for each row of a dataset, defined for each row as:
        relative error = |ground truth count - dp count| / ground truth count
    
    If the ground truth count is 0, this will return null.
    
    inputs:
        df:        the dataframe to calculate relative error for
        exact_col: the name of the column with an exact count
        noisy_col: the name of the column with a noisy count
        
    outputs:
        df with rel_err column
    '''
    return df.withColumn('rel_err', sf.abs(sf.col(exact_col) - sf.col(noisy_col)) / sf.col(exact_col))

def calc_error(df,
               spark,
               table,
               exact_col,
               noisy_col,
               pub_thresh,
               exact_threshs,
               rel_err_threshs,
               year, month, day,
               args):
    print(args)
    # get total count of pages with more than 0 real pageviews
    total_count = df.filter(df[exact_col] > 0).count()
    df_size = df.count()
    
    # precalculate/cache published values, not published values, and published values w/o nulls
    published = df.filter(df[noisy_col] > pub_thresh)
    published.cache()
    published.take(1)
    pub_size = published.count()
    
    not_published = df.filter(df[noisy_col] <= pub_thresh)
    not_published.cache()
    not_published.take(1)
    
    published_no_null = published.filter(published[exact_col] > 0) # remove null rel_err values
    published_no_null.cache()
    published_no_null.take(1)
    pub_no_null_size = published_no_null.count()

    pct_rel_err = []
    pct_dropped = []
    
    # get count of data points released at pub_thresh
    count = published.count()
        
    # calculate percent of spurious published data
    pct_spurious = published.filter(published[exact_col] == 0).count() / pub_size
    
    # calculate percent of all data with true value >= e dropped
    for e in exact_threshs:
        pct_dropped.append(not_published.filter(not_published[exact_col] >= e).count() / df_size)

    med_abs_err = published.approxQuantile('abs_err', [0.5], 0)[0]
        
    if pub_no_null_size > 0:
        # get median relative and abs error
        med_rel_err = published_no_null.approxQuantile('rel_err', [0.5], 0)[0]
#         med_abs_err = published_no_null.approxQuantile('abs_err', [0.5], 0)[0]

        # calculate overall bias
        [dp, no_dp] = published.select([noisy_col, exact_col]).groupBy().sum().collect()[0]
        bias = (dp - no_dp) / no_dp

        # calculate percent of applicable data with relative error <= re threshold
        for re in rel_err_threshs:
            pct_rel_err.append(published_no_null.filter(published_no_null['rel_err'] <= re).count() / pub_size)

    else:
        med_rel_err = -1
#         med_abs_err = -1
        bias = -1
        for re in rel_err_threshs:
            pct_rel_err.append(0)
        
    spark.sql(f'''
        INSERT INTO TABLE differential_privacy.{table}
        VALUES (
            '{"','".join(args)}',
            {total_count},
            {count},
            {med_abs_err},
            {med_rel_err},
            {",".join([str(i) for i in pct_rel_err])},
            {pct_spurious},
            {",".join([str(i) for i in pct_dropped])},
            {bias},
            {year},
            {month},
            {day}
        )
    ''')
    
def calc_error_country_project_page(df, spark, year, month, day):
    table = 'country_language_page_error'
    exact_col = 'count'
    noisy_col = 'gbc'
    pub_thresh = 89.87
    exact_threshs = [1, 150]
    rel_err_threshs = [0.1, 0.25, 0.5]
    
    geo = {
        'all': [],
        'subcontinent': [
            'Eastern Europe',
            'Northern Europe',
            'Eastern Africa',
            'Central America',
            'Western Europe',
            'Caribbean',
            'South-eastern Asia',
            'Eastern Asia',
            'Australia and New Zealand',
            'Southern Asia',
            'Polynesia',
            'Northern America',
            'South America',
            'Southern Europe',
            'Western Africa',
            'Micronesia',
            'Western Asia',
            'Northern Africa',
            'Southern Africa',
            'Middle Africa',
            'Melanesia',
            'Central Asia'
        ],
        'continent': [
            'Europe',
            'Oceania',
            'Latin America and the Caribbean',
            'Asia',
            'Africa',
            'Northern America'
        ]
    }
    
    # calculate relative and absolute errors for each row
    df = calc_rel_err(df=df, exact_col=exact_col, noisy_col=noisy_col)
    df = calc_abs_err(df=df, exact_col=exact_col, noisy_col=noisy_col)
    
    # cache and take(1) for increased speed
    df.cache()
    df.take(1)
    
    # iterate through geo_items
    for geo_level, geo_names in geo.items():
        if geo_level == 'all':
            calc_error(df=df,
                       spark=spark,
                       table=table,
                       exact_col=exact_col,
                       noisy_col=noisy_col,
                       pub_thresh=pub_thresh,
                       exact_threshs=exact_threshs,
                       rel_err_threshs=rel_err_threshs,
                       year=year,
                       month=month,
                       day=day,
                       args=['global', 'global'])
        else:
            for name in geo_names:
                df_filtered = df.filter(df[geo_level] == name)
                calc_error(df=df_filtered,
                           spark=spark,
                           table=table,
                           exact_col=exact_col,
                           noisy_col=noisy_col,
                           pub_thresh=pub_thresh,
                           exact_threshs=exact_threshs,
                           rel_err_threshs=rel_err_threshs,
                           year=year,
                           month=month,
                           day=day,
                           args=[geo_level, name])
