from pyspark.sql import functions as sf
import math

def calc_abs_err(df, exact_col='count', noisy_col='gbc'):
    return df.withColumn('abs_err', sf.abs(sf.col(exact_col) - sf.col(noisy_col)))

def calc_rel_err(df, exact_col='count', noisy_col='gbc'):
    '''
    Calculates relative error for each row of a dataset, defined for each row as:
        relative error = |ground truth count - dp count| / ground truth count
    
    If the ground truth count is 0, this will return null.
    
    inputs:
        df:        the dataframe to calculate relative error for
        exact_col: the name of the column with an exact count
        noisy_col: the name of the column with a noisy count
        
    outputs:
        df with rel_err column
    '''
    return df.withColumn('rel_err', sf.abs(sf.col(exact_col) - sf.col(noisy_col)) / sf.col(exact_col))
        
def total_published(df, noisy_col='gbc', publish_thresh=0):
    '''
    Calculates the total number of noisy counts above publishing threshold.
    
    inputs:
        df:             the dataframe to calculate average relative error for
        noisy_col:      the name of the column with a noisy count
        publish_thresh: threshold above which pageview counts are published
    '''
    return df.filter(f'{noisy_col} > {publish_thresh}').count()
    
def avg_rel_err(df, exact_col='count', noisy_col='gbc', publish_thresh=0):
    '''
    Calculates average relative error across the entire dataframe above publishing threshold.
    Note that average relative error can return numbers greater than 1 if most pages have more
    noise added/subtracted than their actual pagecounts.
    
    inputs:
        df:             the dataframe to calculate average relative error for
        exact_col:      the name of the column with an exact count
        noisy_col:      the name of the column with a noisy count
        publish_thresh: threshold above which pageview counts are published
    
    outputs:
        mean of rel_err column
    '''
    return (
        df.filter(f'{noisy_col} > {publish_thresh} AND {exact_col} > 0') # remove null rel_err values
        .approxQuantile('rel_err', [0.5], 1e-6)[0]
    )

def pct_rel_err(df, exact_col='count', noisy_col='gbc', publish_thresh=0, rel_err_thresh=0.05):
    '''
    Calculates percentage of noisy counts above publishing threshold that are within some percentage
    of exact counts.
    
    inputs:
        df:             the dataframe to calculate percentage of relative errors within threshold for
        exact_col:      the name of the column with an exact count
        noisy_col:      the name of the column with a noisy count
        publish_thresh: threshold above which pageview counts are published
        rel_err_thresh: threshold for amount of acceptable relative error 
    
    outputs:
        mean of rel_err column
    '''
    published = df.filter(f'{noisy_col} > {publish_thresh} AND {exact_col} > 0') # remove null rel_err values
    return (
        published.filter(f'rel_err <= {rel_err_thresh}').count() / published.count()
    )

def drop_rate(df, exact_col='count', noisy_col='gbc', publish_thresh=0, exact_thresh=0):
    '''
    Calculates the drop rate for a given threshold, defined as:
        drop rate = count(noisy counts <= publish thresh AND exact counts > 0) / count(exact counts > 0)
            
    inputs:
        df:             the dataframe to calculate drop rate for
        exact_col:      the name of the column with an exact count
        noisy_col:      the name of the column with a noisy count
        publish_thresh: threshold above which pageview counts are published
    
    outputs:
        drop rate
    '''
    non_published = df.filter(f'{noisy_col} <= {publish_thresh} AND {exact_col} > {exact_thresh}')
    greater_than_zero = df.filter(f'{exact_col} > 0')
    return non_published.count() / greater_than_zero.count()

def spurious_pv_rate(df, exact_col='count', noisy_col='gbc', publish_thresh=0):
    '''
    Calculates percentage of the published *pageviews* that are "spurious", i.e. pages with 0 views in the
    ground truth dataset but >threshold views in the dp dataset.
    
    inputs:
        df:             the dataframe to calculate spurious pageview rate for
        exact_col:      the name of the column with an exact count
        noisy_col:      the name of the column with a noisy count
        publish_thresh: threshold above which pageview counts are published
    
    outputs:
        spurious pageview rate
    '''
    u_bar = df.filter(f'{exact_col} == 0 AND {noisy_col} > {publish_thresh}')
    published = df.filter(f'{noisy_col} > {publish_thresh}')
    return u_bar.select(sf.sum(noisy_col)).collect()[0][0] / published.select(sf.sum(noisy_col)).collect()[0][0]
    
def spurious_page_rate(df, exact_col='count', noisy_col='gbc', publish_thresh=0):
    '''
    Calculates percentage of the published *pages* that are "spurious", i.e. pages with 0 views in the
    ground truth dataset but >threshold views in the dp dataset.
    
    inputs:
        df:             the dataframe to calculate spurious pageview rate for
        exact_col:      the name of the column with an exact count
        noisy_col:      the name of the column with a noisy count
        publish_thresh: threshold above which pageview counts are published
    
    outputs:
        spurious pageview rate
    '''
    u_bar = df.filter(f'{exact_col} == 0 AND {noisy_col} > {publish_thresh}')
    published = df.filter(f'{noisy_col} > {publish_thresh}')
    return u_bar.count() / published.count()

def calc_metrics(df, publish_thresholds, rel_err_thresholds, exact_thresholds):
    '''
    Calculate all metrics for a given df for a set of publishing thresholds and relative errror thresholds
    '''
    metrics = {}
    for p in publish_thresholds:
        metrics[p] = {}
        metrics[p]['total_published'] = total_published(df=df,
                                                        publish_thresh=p)
        metrics[p]['avg_rel_err'] = avg_rel_err(df=df,
                                                publish_thresh=p)
        for e in exact_thresholds:
            metrics[p][f'drop_rate_{e}'] = drop_rate(df=df,
                                                     publish_thresh=p,
                                                     exact_thresh=e)
        try:
            metrics[p]['spurious_pv_rate'] = spurious_pv_rate(df=df,
                                                              publish_thresh=p)
        except:
            metrics[p]['spurious_pv_rate'] = None
        try:
            metrics[p]['spurious_page_rate'] = spurious_page_rate(df=df,
                                                                  publish_thresh=p)
        except:
            metrics[p]['spurious_page_rate'] = None
        for r in rel_err_thresholds:
            metrics[p][f'pct_rel_err_{r}'] = pct_rel_err(df=df,
                                                         publish_thresh=p,
                                                         rel_err_thresh=r)
    return metrics

def calc_thresholds(rel_err, epsilon, contrib_thresh):
    lo_thresh = (contrib_thresh / (rel_err*epsilon)) * math.log(20)
    hi_thresh = ((1 + rel_err) / rel_err) * (contrib_thresh / epsilon) * math.log(20)
    return [lo_thresh, hi_thresh]
