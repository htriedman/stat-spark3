# stat-spark3

A collection of hacks to run DIY spark3 binaries in Jupyter Lab on the Analytics infra. The goal of this project is to allow for the deployment of Tumult Labs' software onto our infrastructure so that we can do differentially private data releases.

WARNING: this is *not* an official project, and no support is provided. Expect things to break.

# What

The project `Makefile` inits a Spark3 project under `{pwd}/spark3`.

It installs Spark3 with Metastore (Hive) integration, and provides a Jupyter Lab instance,
with pyspark support, kernel inside a virtual environment.

## Caveats

Dynamic allocation and shuffle service must be turned off:
```
spark.dynamicAllocation.enabled                     false
spark.shuffle.service.enabled                       false
```

`spark.properties` provides a basic config, modified from the default 2.4 one.

# Getting started

Clone this repo on a target host (e.g. stat1006.eqiad.wmnet), and hop onto it over ssh.


Initialize the project with:
```
make install
```

Launch a jupyter lab server with:
```
make jupyter
```

This command should produce something like:
```
[...]
    To access the server, open this file in a browser:
        file:///srv/home/<username>/.local/share/jupyter/runtime/jpserver-8432-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/lab?token=<token>
     or http://127.0.0.1:8888/lab?token=<token>
```

You'll need to establish a ssh tunnel from your local machine to the jupyter lab instance.
For example (from your laptop
```
ssh -t -N -L 8888:127.0.0.1:8888 stat1006.eqiad.wmnet
```
Now you can access http://localhost:8888/lab?token=<token> from the browser.

# Uninstall

The following command will get rid of project files, python venv, and spark3 binaries:
```
make uninstall
```

# Update

The following command will uninstall and reinstall the package:
```
make update
```

## Deprecated scripts

Run `./scripts/install_spark3.sh` to install spark3 and necessary dependencies.

Run `./scripts/start_spark3_notebook.sh` if you've already installed spark3 and necessary dependencies to start a Jupyter notebook using spark3.

Run `./scripts/install_and_start_spark3.sh` to both install spark3 and start a notebook with it.
