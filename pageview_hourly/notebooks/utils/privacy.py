from datetime import date
from math import log, sqrt

global_priv_register = 'differential_privacy.global_privacy_register'

def gen_rho(epsilon, delta):
    logterm = log(1 / delta)
    # Theorem 3.5 in https://arxiv.org/pdf/1605.02065.pdf
    # This is not tight, but should be a good enough approximation for experimentation
    return (sqrt(epsilon+logterm) - sqrt(logterm))**2

def date_to_str(year, month, day):
    return date(year=year, month=month, day=day).strftime("%Y-%m-%d")

def write_to_global_privacy_budget(spark, table, epsilon, year, month, day, delta=0, rho=None):
    if rho is None:
        rho = gen_rho(epsilon, delta)
    
    spark.sql(f'''
    INSERT INTO TABLE {global_priv_register}
    VALUES
        ("{table}", {epsilon}, {delta}, {rho}, "{date_to_str(year, month, day)}", {year}, {month}, {day})
    ''')
