#!/bin/bash

# activate `tumult` venv and start a jupyter notebook
# It is expected that the venv has been created
# with pack_python_env.sh
source /opt/conda-analytics/etc/profile.d/conda.sh

# start venv
export venv=tumult
conda activate ${venv}

# print instructions and start jupyter notebook
printf "\n\nNow do the following:\n 1. Open a new local terminal instance and run 'ssh -N $(hostname).eqiad.wmnet -L 8888:127.0.0.1:8888'. You may be prompted to log in, and then the command should hang.\n 2. Open your local browser and copy-paste the url below that looks like 'https://127.0.0.1:8888/?token=<random gibberish>'\n 3. Now you should be in JupyterLab. Make a duplicate of 'Basic Spark3 Notebook Recipe.ipynb', put it into 'notebooks/' and run the cells in there to start and connect to pyspark in a Jupyter Notebook.\n\n"

jupyter notebook --no-browser

# deactivate venv
conda deactivate
