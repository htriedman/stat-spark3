#!/bin/bash

# activate `tumult` venv.
# It is expected that the venv has been created 
# with pack_python_env.sh
# TODO(gmodena): here we are abusing stacked conda to init 
# a shell without touching the user .basrhc. It feels kind of iffy
# and will need some futher testing.
python_version=3.7
export venv=tumult
source /usr/lib/anaconda-wmf/bin/conda-activate-stacked ${venv}
# spark configs
## Spark is installed in the tumult conda environment.
export SPARK_HOME=${HOME}/.conda/envs/${venv}/lib/python${python_version}/site-packages/pyspark
## Configs under SPARK_CONF_DIR will override defaults.
## Since SPARK_HOME points to a directory in PYTHONPATH, we keep relevant
## config files in a user owned path.
export SPARK_CONF_DIR=$(echo ${HOME})/stat-spark3/conf/
# export SPARK_CONF_DIR=/etc/spark3/conf/
export HADOOP_CONF_DIR=/etc/hadoop/conf/
export PYSPARK_SUBMIT_ARGS="pyspark-shell"
# export HIVE_CONF_DIR=/etc/spark2/conf/
export HIVE_CONF_DIR=/etc/spark3/conf/

# to open spark shell, run:
# $SPARK_HOME/bin/pyspark --properties-file $(pwd)/spark.properties

# print instructions and start jupyter notebook
printf "\n\nNow do the following:\n 1. Open a new local terminal instance and run 'ssh -N $(hostname).eqiad.wmnet -L 8888:127.0.0.1:8888'. You may be prompted to log in, and then the command should hang.\n 2. Open your local browser and copy-paste the url below that looks like 'https://127.0.0.1:8888/?token=<random gibberish>'\n 3. Now you should be in JupyterLab. Make a duplicate of 'Basic Spark3 Notebook Recipe.ipynb', put it into 'notebooks/' and run the cells in there to start and connect to pyspark in a Jupyter Notebook.\n\n"

jupyter notebook --no-browser

source /usr/lib/anaconda-wmf/bin/conda-deactivate-stacked
