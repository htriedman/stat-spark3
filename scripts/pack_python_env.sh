#!/usr/bin/env bash
#
# Run with:
# $ ./pack_python_env.sh
#
# The environment will be create under ${HOME}/.conda.
# It can be activated with
# $ source source /opt/conda-analytics/etc/profile.d/conda.sh
# $ conda activate tumult
#
source /opt/conda-analytics/etc/profile.d/conda.sh

export http_proxy=http://webproxy.eqiad.wmnet:8080
export https_proxy=http://webproxy.eqiad.wmnet:8080

conda env create -n tumult -f conda-environment.yml
#conda-analytics-clone tumult
#source conda-analytics-activate tumult
#pip3 install tmlt-analytics==0.8
#source conda-analytics-deactivate
