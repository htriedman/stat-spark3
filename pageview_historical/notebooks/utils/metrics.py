from pyspark.sql import functions as sf
import math

geo = {
    'all': [],
    'subcontinent': [
        ('Eastern Europe', None),
        ('Northern Europe', None),
        ('Eastern Africa', None),
        ('Central America', None),
        ('Western Europe', None),
        ('Caribbean', None),
        ('South-eastern Asia', None),
        ('Eastern Asia', None),
        ('Australia and New Zealand', None),
        ('Southern Asia', None),
        ('Polynesia', None),
        ('Northern America', None),
        ('South America', None),
        ('Southern Europe', None),
        ('Western Africa', None),
        ('Micronesia', None),
        ('Western Asia', None),
        ('Northern Africa', None),
        ('Southern Africa', None),
        ('Middle Africa', None),
        ('Melanesia', None),
        ('Central Asia', None)
    ],
    'country_project': [
        # large
        ('United States of America', 'en.wikipedia'),
        ('Germany', 'de.wikipedia'),
        ('Mexico', 'es.wikipedia'),
        ('Brazil', 'pt.wikipedia'),
        ('Taiwan', 'zh.wikipedia'),
        
        # medium
        ('India', 'hi.wikipedia'),
        ('Poland', 'pl.wikipedia'),
        ('Philippines', 'en.wikipedia'),
        ('Sweden', 'sv.wikipedia'),
        ('Indonesia', 'id.wikipedia'),
        
        # small
        ('India', 'ml.wikipedia'),
        ('Peru', 'es.wikipedia'),
        ('Ghana', 'en.wikipedia'),
        ('Greece', 'el.wikipedia'),
        ('United States of America', 'de.wikipedia')
    ]
}

def calc_abs_err(df, exact_col='count', noisy_col='gbc'):
    return df.withColumn('abs_err', sf.abs(sf.col(exact_col) - sf.col(noisy_col)))

# use on entire dataset and intersections
def calc_bias(df, release_thresh, noisy_col, exact_col):
    dp = df.filter(df[noisy_col] >= release_thresh).select(noisy_col).groupBy().sum().collect()[0][0]
    no_dp = df.filter(df[exact_col] >= release_thresh).select(exact_col).groupBy().sum().collect()[0][0]
    return (dp - no_dp) / no_dp

def calc_med_abs_err(df):
    return df.approxQuantile('abs_err', [0.5], 0.01)[0]

# def calc_dropped(df, release_thresh, noisy_col, exact_col):
#     return df.filter((df[noisy_col] < release_thresh) & (df[exact_col] >= release_thresh)).count() / df.count()

# def calc_spurious(df, release_thresh, noisy_col, exact_col):
#     return df.filter((df[noisy_col] >= release_thresh) & (df[exact_col] == 0)).count() / df.count()

def calc_dropped(df, release_thresh, noisy_col, exact_col):
    non_published = df.filter(
        (df[noisy_col] > 0) &
        (df[noisy_col] < release_thresh) &
        (df[exact_col] >= release_thresh)
    )
    should_be_published = df.filter(df[exact_col] >= release_thresh)
    return non_published.count() / should_be_published.count()

def calc_spurious(df, release_thresh, noisy_col, exact_col):
    published = df.filter(df[noisy_col] >= release_thresh)
    return published.filter(published[exact_col] == 0).count() / published.count()

# use only on intersections
def top_k_nonprivate(df, release_thresh, exact_col, geo_level, geo_name, project, k):
    if geo_level == 'subcontinent':
        df = df.filter(df.subcontinent == geo_name)
    elif geo_level == 'country_project':
        df = df.filter(df.country == geo_name).filter(df.project == project)
    
    return (
        df.filter(df[exact_col] >= release_thresh)
        .sort(df[exact_col].desc())
        .limit(k)
    )

def top_private(df, release_thresh, noisy_col, geo_level, geo_name, project):
    # no k because we include all rows above threshold
    if geo_level == 'subcontinent':
        df = df.filter(df.subcontinent == geo_name)
    elif geo_level == 'country_project':
        df = df.filter(df.country == geo_name).filter(df.project == project)
    
    return df.filter(df[noisy_col] >= release_thresh)

def setify(df, kind):
    if kind == 'id':
        return set(
            df.select('page_id')
            .collect()
        )
    else:
        return set(
            df.select('page_title')
            .collect()
        )

def calc_recall(private_set, nonprivate_set):
    return len(private_set & nonprivate_set) / len(nonprivate_set)

def calc_error(
    df, # df to calculate error on
    spark, # spark instance
    table, # output table to write to
    exact_col, # name of column without noise
    noisy_col, # name of column with noise
    release_thresh, # threshold for releasing data
    geo_level, # geo level we're considering (either subcontinent or country)
    geo_name, # geo entity to filter to
    project, # project (if applicable, will be None for subcontinent)
    kind, # either 'id' or 'title' depending on the analysis being done
    year, month, day # date
):
    if geo_level == 'all':
        count = df.filter(df[exact_col] >= release_thresh).count()
        published_count = df.filter(df[noisy_col] >= release_thresh).count()
        recall = calc_recall(
            setify(df.filter(df[noisy_col] >= release_thresh), kind),
            setify(df.filter(df[exact_col] >= release_thresh), kind)
        )
        bias = calc_bias(df, release_thresh, noisy_col, exact_col)
        med_abs_err = calc_med_abs_err(df)
        dropped = calc_dropped(df, release_thresh, noisy_col, exact_col)
        spurious = calc_spurious(df, release_thresh, noisy_col, exact_col)
    else:
        p = top_private(df, release_thresh, noisy_col, geo_level, geo_name, project)
        p.cache()
        np = top_k_nonprivate(df, release_thresh, exact_col, geo_level, geo_name, project, 1000)
        count = np.count()
        published_count = p.count()
        try:
            recall = calc_recall(setify(p, kind), setify(np, kind))
        except:
            recall = -1

        try:
            bias = calc_bias(p, release_thresh, noisy_col, exact_col)
        except:
            bias = -1

        try:
            med_abs_err = calc_med_abs_err(p)
        except:
            med_abs_err = -1
            
        try:
            dropped = calc_dropped(p, release_thresh, noisy_col, exact_col)
        except:
            dropped = -1

        try:
            spurious = calc_spurious(p, release_thresh, noisy_col, exact_col)
        except:
            spurious = -1
    
    spark.sql(f'''
        INSERT INTO TABLE differential_privacy.{table}
        VALUES (
            "{geo_level}",
            "{geo_name}",
            "{project}",
            {count},
            {published_count},
            {recall},
            {bias},
            {med_abs_err},
            {dropped},
            {spurious},
            {year},
            {month},
            {day}
        )
    ''')

def calc_error_pageview_historical(df, spark, year, month, day, release_thresh, kind='id'):
    noisy_col = 'views_sum'
    exact_col = 'views_sum_nonprivate'
    table = 'pageview_historical_error'
    
    df = calc_abs_err(df, noisy_col=noisy_col, exact_col=exact_col)

    # iterate through geo_items
    for geo_level, geo_names in geo.items():
        if geo_level == 'all':
            calc_error(df=df,
                       spark=spark,
                       table=table,
                       exact_col=exact_col,
                       noisy_col=noisy_col,
                       release_thresh=release_thresh,
                       geo_level=geo_level,
                       geo_name=None,
                       project=None,
                       year=year,
                       month=month,
                       day=day,
                       kind=kind)
        else:
            for name in geo_names:
                calc_error(df=df,
                           spark=spark,
                           table=table,
                           exact_col=exact_col,
                           noisy_col=noisy_col,
                           release_thresh=release_thresh,
                           geo_level=geo_level,
                           geo_name=name[0],
                           project=name[1],
                           year=year,
                           month=month,
                           day=day,
                           kind=kind)
