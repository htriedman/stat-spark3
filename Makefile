SHELL := /bin/bash
venv := tumult

venv:
	./scripts/pack_python_env.sh

install: venv

uninstall:
	rm -rf ${HOME}/.local
	rm ${HOME}/.conda/environments.txt
	rm -rf ${HOME}/.conda/envs/${venv}

update: uninstall install

jupyter:
	./scripts/start_spark3_notebook.sh
