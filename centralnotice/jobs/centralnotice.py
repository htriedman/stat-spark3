# Imports# System packages
import os
import re
import argparse

import pandas as pd
import numpy as np

from wmfdata.spark import create_custom_session

# Pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import split, col, abs as sp_abs, sum as sp_sum, to_timestamp
from pyspark.sql.types import StringType

# Tumult Analytics
from tmlt.analytics.keyset import KeySet
from tmlt.analytics.protected_change import AddRowsWithID, AddOneRow
from tmlt.analytics.privacy_budget import PureDPBudget
from tmlt.analytics.query_builder import QueryBuilder
from tmlt.analytics.session import Session
from tmlt.analytics.constraints import MaxRowsPerID

# WMF packages
import mwapi
import mwparserfromhell

#------------ CONSTANTS ------------#
IMP_EPSILON = 1
CLICK_EPSILON = 2
CLICK_TRUNCATION = 1
CLICK_SECONDS = 180

class Args:
    year = 2023
    month = 7
    day = 12

#------------ SQL QUERIES ------------#

# Retrieving user browser activity related to CentralNotice banners
# uri_query has CentralNotice information such as banner name, campaign name, etc.
webrequest_query = """
SELECT
    wr.dt,
    wr.uri_query,
    wr.ip,
    wr.user_agent
FROM
    wmf.webrequest as wr
RIGHT JOIN htriedman.non_country_protection_list AS ncpl
    ON wr.geocoded_data["country"] = ncpl.country_name
WHERE
    year = {year}
    AND month = {month}
    AND day = {day}
    AND webrequest_source = 'text'
    -- drop requests with no timestamps
    AND dt != '-'
    AND uri_path = '/beacon/impression'
    AND agent_type = 'user'
"""

# Retrieving datetime, IP address, actor_signature, page title, user_agent, referer class, and country from wmf.pageview_actor
# Joining on htriedman.non_country_protection_list
# wmf.webrequest and pageview_actor will be joined together to get more info about CentralNotice banner interactions
pageview_actor_query = """
SELECT
    pa.dt,
    pa.ip,
    pa.actor_signature,
    pa.pageview_info["page_title"],
    pa.user_agent,
    pa.referer_class,
    pa.geocoded_data["country"]
FROM
    wmf.pageview_actor as pa
RIGHT JOIN htriedman.non_country_protection_list AS ncpl
    ON pa.geocoded_data["country"] = ncpl.country_name
WHERE
    year = {year}
    AND month = {month}
    AND day = {day}
    AND referer_class = 'internal'
    AND dt != '-'
    AND agent_type = 'user'
"""

# All countries whose data can be released
country_query = """
SELECT country_name, country_code AS country
FROM htriedman.non_country_protection_list
"""

#------------ HELPER FUNCTIONS (PREPROCESSING) -------------#
# Returns ALL links from campaign request page
def get_links(session, title):
    resp = session.get(
        formatversion=2,
        action='query',
        prop='revisions',
        rvslots='*',
        rvprop='content',
        titles=f'CentralNotice/Request/{title}'
    )

    content = resp["query"]["pages"][0]["revisions"][0]["slots"]["main"]["content"]

    links = re.findall("\[\[([^\|]+?)\]\]|\[\[(.+?)\|.*?\]\]", content)
    links_parsed = []
    for l in links:
        if l[0] == "":
            links_parsed.append(l[1])
        else:
            links_parsed.append(l[0])

    return links_parsed

# Removes unwanted links
def remove(link):
    if "user:" in link.lower():
        return False
    return True

# Removes unwanted prefixes from links
def remove_prefix(link):
    wanted_prefixes = ["research", "talk", "commons", "wikipédia", "wikipedia", "grants", "project"]
    output = ""
    link_parts = link.split(":")
    for part in link_parts:
        if part.lower() in wanted_prefixes:
            output += part + ":"
    output += link_parts[-1]
    return output

def get_cn_pages(spark):
    # Using MediaWiki API to parse CN banner request list
    session = mwapi.Session("https://meta.wikimedia.org", user_agent="centralnotice DP daily")
    resp = session.get(
        formatversion=2,
        action='query',
        prop='revisions',
        rvslots='*',
        rvprop='content',
        titles='CentralNotice/Request/List'
    )
    content = resp["query"]["pages"][0]["revisions"][0]["slots"]["main"]["content"]

    # Use mwparserfromhell to parse the content and retrieve templates
    wikitext = mwparserfromhell.parse(content)
    t = wikitext.filter_templates()

    # Parsing titles of campaigns
    campaign_titles = []
    for i in t[1:]:
        campaign_titles.append(i.split("request=")[1].split("|")[0])

    # Returns ALL the links from EACH campaign request page
    all_links = []
    for i in campaign_titles:
        all_links.append(get_links(session, i))
    # Turning nested list into a single list
    all_links = sum(all_links, [])

    # Using remove() on all_links
    links_filtered = []
    for l in filter(remove, all_links):
        links_filtered.append(l)

    # Removing duplicate links from list
    links_set = set(links_filtered)
    links_filtered = list(links_set)

    # Using remove_prefix() on links_filtered and add Talk/Special:MyLanguage
    links = []
    for l in links_filtered:
        l = remove_prefix(l)
        l = l.replace(" ", "_")
        links += [l, f"Talk:{l}", f"Special:MyLanguage/{l}"]

    # Turning the links into a dataframe
    schema = ['page_title']
    links_df = spark.createDataFrame(links, StringType())
    links_df = links_df.withColumnRenamed("value", "page_title")
    return links_df

#------------ CentralNotice Data Preprocessing Functions ------------#
def preprocess_impressions(spark, args):

    # Using Spark to create dataframes from the above queries
    webrequest_df = spark.sql(webrequest_query.format(year=args.year, month=args.month, day=args.day))

    # Data cleaning

    # Retrieving relevant columns by splitting uri_query
    webrequest_df = (
        webrequest_df.withColumn('statusCode', split(webrequest_df['uri_query'], '&statusCode=').getItem(1))
            .withColumn('campaign', split(webrequest_df['uri_query'], '&campaign=').getItem(1))
            .withColumn('campaign_category', split(webrequest_df['uri_query'], '&campaignCategory=').getItem(1))
            .withColumn('banner', split(webrequest_df['uri_query'], '&banner=').getItem(1))
            .withColumn('project', split(webrequest_df['uri_query'], '&project=').getItem(1))
            .withColumn('country', split(webrequest_df['uri_query'], 'country=').getItem(1))
            .withColumn('user_lang', split(webrequest_df['uri_query'], 'uselang=').getItem(1))
            .withColumn('debug', split(webrequest_df['uri_query'], '&debug=').getItem(1))
    )

    # Removing extraneous "&" character from all relevant arguments
    webrequest_df = (
        webrequest_df.withColumn('statusCode', split(webrequest_df['statusCode'], '&').getItem(0))
            .withColumn('campaign', split(webrequest_df['campaign'], '&').getItem(0))
            .withColumn('campaign_category', split(webrequest_df['campaign_category'], '&').getItem(0))
            .withColumn('banner', split(webrequest_df['banner'], '&').getItem(0))
            .withColumn('project', split(webrequest_df['project'], '&').getItem(0))
            .withColumn('country', split(webrequest_df['country'], '&').getItem(0))
            .withColumn('user_lang', split(webrequest_df['user_lang'], '&').getItem(0))
            .withColumn('debug', split(webrequest_df['debug'], '&').getItem(0))
    )

    # Filter to successful (statusCode = 6), non-debugging, and non-fundraising requests
    webrequest_df = webrequest_df.filter(webrequest_df.statusCode == 6)
    webrequest_df = webrequest_df.filter(webrequest_df.debug == 'false')
    webrequest_df = webrequest_df.filter(~col("campaign_category").contains("fundraising"))

    # drop unneeded rows
    webrequest_df = (
        webrequest_df.drop("uri_query")
        .drop("debug")
        .drop("statusCode")
    )

    return webrequest_df

def preprocess_clicks(spark, args, impressions_df):
    # Using Spark to create dataframes from the above queries
    pageview_actor_df = spark.sql(pageview_actor_query.format(year=args.year, month=args.month, day=args.day))

    # Rename column to make sure we don't have duplicate columns named "dt"
    pageview_actor_df = pageview_actor_df.withColumnRenamed("dt", "dt_follow")

    # Joining webrequest and pageview_actor tables on IP address and user_agent (basically actor_signature_
    join_df = impressions_df.join(pageview_actor_df, on=["ip", "user_agent"]).drop("ip").drop("user_agent")
    join_df.cache()

    # Calculating difference in seconds between a CN banner view and following page view
    join_df = join_df.withColumn("time_diff", to_timestamp(col("dt_follow")).cast("long") - to_timestamp(col("dt")).cast("long"))

    # Filtering to only pages viewed CLICK_SECONDS minutes after seeing a CN banner
    join_df = join_df.filter(join_df.time_diff > 0).filter(join_df.time_diff < CLICK_SECONDS)

    # Drop unneeded columns
    join_df = join_df.drop("referer_class").drop("time_diff").drop("dt_follow")  


    # Reorganize the columns of the dataframe
    join_df = join_df.withColumnRenamed("pageview_info[page_title]", "page_title")
    join_df = join_df.select("dt", "page_title", "banner", "campaign", "campaign_category", "project", "country", "geocoded_data[country]", "user_lang", "actor_signature")

    links_df = get_cn_pages(spark)

    # Final matching links dataframe
    matching_pages = join_df.join(links_df, on="page_title")

    return matching_pages

# # Error calculations
# def calculate_error(df, exact_col, noisy_col, args):
#     df = (
#         df
#         .withColumn('abs_error', (sp_abs(df[exact_col] - df[noisy_col])))
#         .withColumn('rel_error', (sp_abs(df[exact_col] - df[noisy_col])) / df[exact_col])
#     )

#     # Median absolute error
#     med_abs_error = df.approxQuantile('abs_error', [0.5], .0001)[0]

#     # Median relative error
#     med_rel_error = df.approxQuantile('rel_error', [0.5], .0001)[0]

#     # Bias
#     private_sum = df.select(sp_sum(df[noisy_col])).collect()[0][0]
#     non_private_sum = df.select(sp_sum(df[exact_col])).collect()[0][0]
#     bias = abs(non_private_sum - private_sum) / non_private_sum

#     args['med_abs_error'] = med_abs_error
#     args['med_rel_error'] = med_rel_error
#     args['bias'] = bias

#     return args

#------------ START MAIN FUNCTION ------------#

def run_dp(args, spark, log):
    log.info("Getting impressions data...")
    # Get impressions
    impressions = preprocess_impressions(spark, args)

    log.info("Getting clicks data...")
    # Get clicks
    clicks = preprocess_clicks(spark, args, impressions)

    log.info("Defining keyset...")
    # Define key_df and keyset (public already so we can use the distinct() function)
    key_df = impressions.select(["country", "user_lang", "project", "campaign", "banner"]).distinct()
    ks = KeySet.from_dataframe(key_df)

    log.info("Defining impressions session and query...")
    # Define impressions session and query
    imp_session = Session.from_dataframe(
        privacy_budget=PureDPBudget(IMP_EPSILON),
        source_id="impressions",
        dataframe=impressions,
        protected_change=AddOneRow(),
    )
    imp_query = (
        QueryBuilder('impressions')
        .groupby(ks)
        .count()
    )

    log.info("Running impressions aggregation...")
    # Run impressions aggregation
    imp_private = imp_session.evaluate(imp_query, PureDPBudget(epsilon=IMP_EPSILON))

    log.info("Defining clicks session and query...")
    # Define clicks session and query
    click_session = Session.from_dataframe(
        privacy_budget=PureDPBudget(CLICK_EPSILON),
        source_id="clicks",
        dataframe=clicks,
        protected_change=AddRowsWithID(id_column="actor_signature"),
    )
    click_query = (
        QueryBuilder('clicks')
        .enforce(MaxRowsPerID(CLICK_TRUNCATION))
        .groupby(ks)
        .count()
    )
    log.info("Running clicks aggregation...")
    # Run clicks aggregation
    click_private = click_session.evaluate(click_query, PureDPBudget(epsilon=CLICK_EPSILON))

    log.info("Doing non-private aggregations...")
    # Group-by and count for non-private aggregation
    imp_nonprivate = (
        impressions.groupby("country","user_lang", "project", "campaign", "banner").count()
        .withColumnRenamed("count", "non_private_count")
    )
    click_nonprivate = (
        clicks.groupby("country","user_lang", "project", "campaign", "banner").count()
        .withColumnRenamed("count", "non_private_count")
    )

    log.info("Joining private and non-private dfs for error calculation...")
    # Join private and non-private dfs for error calculation
    imp_joined = imp_private.join(
        imp_nonprivate,
        (imp_nonprivate["banner"] == imp_private["banner"]) &
        (imp_nonprivate["campaign"] == imp_private["campaign"]) &
        (imp_nonprivate["project"] == imp_private["project"]) &
        (imp_nonprivate["user_lang"] == imp_private["user_lang"]) &
        (imp_nonprivate["country"] == imp_private["country"])
    )
    click_joined = click_private.join(
        click_nonprivate,
        (click_nonprivate["banner"] == click_private["banner"]) &
        (click_nonprivate["campaign"] == click_private["campaign"]) &
        (click_nonprivate["project"] == click_private["project"]) &
        (click_nonprivate["user_lang"] == click_private["user_lang"]) &
        (click_nonprivate["country"] == click_private["country"])
    )

    # At this point, we would want to calculate error and write to the global privacy register
    log.info("Calculating error...")
    log.info("Writing to global privacy register...")

    # Write to out table
    log.info("Writing to hive...")
    imp_private.write.saveAsTable("htriedman.cluster_cn_imp_2023_07_12")
    click_private.write.saveAsTable("htriedman.cluster_cn_click_2023_07_12")


def main():
    args = Args()

    # Spark Cluster
    spark = SparkSession.builder.getOrCreate()
    log = spark.sparkContext._jvm.org.apache.log4j.LogManager.getLogger(__name__)
    
    run_dp(args=args, spark=spark, log=log)
    
    # cleanup tumult analytics and spark cluster
    log.info("cleaning up tmlt.analytics and stopping spark...")
    cleanup()
    spark.sparkContext.stop()
    spark.stop()
    
if __name__ == "__main__":
    main()
