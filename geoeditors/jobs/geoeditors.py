# system packages
import os
# import sys
from datetime import datetime, timedelta

# sys.path.append(".")

# from utils.metrics import calc_error_geoeditors
import pandas as pd

# pyspark
from pyspark.sql import SparkSession
from pyspark.sql import functions as sf
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DoubleType
from pyspark.ml.feature import Bucketizer

# tumult analytics
from tmlt.analytics.privacy_budget import PureDPBudget
from tmlt.analytics.query_builder import QueryBuilder
from tmlt.analytics.session import Session
from tmlt.analytics.keyset import KeySet
from tmlt.analytics.binning_spec import BinningSpec
from tmlt.analytics.protected_change import AddOneRow

# tumult core
from tmlt.core.utils.cleanup import cleanup

#------------ START SQL QUERIES ------------#
# all edits for a month from editors_daily
monthly_edit_query = """
SELECT
  wiki_db,
  country_code,
  user_fingerprint_or_name as user,
  CAST(SUM(edit_count) AS FLOAT) as sum_edits,
  -- CAST(SUM(namespace_zero_edit_count) AS FLOAT) as sum_ns0_edits,
  month
FROM
  wmf.editors_daily
WHERE
  month = '{month}'
  -- Filter out bot actions and non-edit actions
  AND size(user_is_bot_by) = 0
  AND action_type IN (0, 1)
GROUP BY
  wiki_db,
  country_code,
  user_fingerprint_or_name,
  month
"""

# all country-wikidb combos we are calculating noise for
keyset_query = """
SELECT
  ncpl.country_code as country_code,
  w.database_code as wiki_db
FROM htriedman.non_country_protection_list ncpl
CROSS JOIN canonical_data.wikis w
WHERE
  w.status = 'open'
  AND w.visibility = 'public'
  AND w.editability = 'public'
  AND w.database_group IN (
    'wikipedia',   -- 320 instances
    'wiktionary',  -- 166 instances
    'wikibooks',   --  77 instances
    'wikiquote',   --  72 instances
    'wikisource',  --  72 instances
    'wikinews',    --  30 instances
    'wikivoyage',  --  25 instances
    'wikiversity', --  17 instances
    'commons',     --   1 instance
    'wikidata',    --   1 instance
    'meta',        --   1 instance
    'mediawiki'    --   1 instance
    -- total          783 instances (as of June 2023)
  )
"""
#------------ END SQL QUERIES ------------#

#------------ START HYPERPARAMETERS AND CONSTANTS ------------#
# epsilons and release thresh
initial_eps = 1.1 # for first query, which will define keyset for future queries
subsequent_eps = 0.9 # for future queries
release_thresh = 8 # for the first query, which will define keyset for future queries

# binning-related things
bin_col = 'edit_range' # name of column with ranges
splits = [0., 4., 99., float('inf')]
edit_binspec = BinningSpec(splits)
labels = edit_binspec.bins()

# short-hand lists to save space referring to further down
# input_cols = ['sum_edits', 'sum_ns0_edits']
# private_output_cols = ['private_count', 'private_sum', 'private_count_ns0', 'private_sum_ns0']
# nonprivate_output_cols = ['nonprivate_count', 'nonprivate_sum', 'nonprivate_count_ns0', 'nonprivate_sum_ns0']
input_cols = ['sum_edits']
private_output_cols = ['private_count', 'private_sum']
nonprivate_output_cols = ['nonprivate_count', 'nonprivate_sum']

# keys to join private/nonprivate dfs on for error calculation
join_keys = ['country_code', 'wiki_db', bin_col]

# error schema for writing to global privacy register
error_schema = ['name', 'released', 'med_rel_err', 'dropped', 'spurious', 'rel_err_fraction_0_5', 'rel_err_fraction_0_9', 'total_inconsistent', 'pct_inconsistent', 'week_start']
loss_schema = StructType([       
        StructField('table', StringType(), False),
        StructField('epsilon', DoubleType(), False),
        StructField('delta', DoubleType(), True),
        StructField('rho', DoubleType(), True),
        StructField('date', StringType(), False),
        StructField('year', IntegerType(), False),
        StructField('month', IntegerType(), False),
        StructField('day', IntegerType(), False)
])

# todo: replace this later
class GeoEditorsArgs:
    def __init__(self, time_start, output_table):
        self.time_start = time_start
        self.output_table = output_table
        
#------------ END HYPERPARAMETERS AND CONSTANTS ------------#

#------------ START HELPER FUNCTIONS ------------#
# Assigns buckets to raw edit counts for apples-to-apples error calculation 
def bucketize(df, input_col, bin_col):
#     labels = edit_binspec.bins()
    # Create a bucketizer instance
    bucketizer = Bucketizer(splits=splits, inputCol=input_col, outputCol=bin_col)

    # Apply bucketizer to the DataFrame
    df = bucketizer.transform(df)

    # Replace the "age_range" column with "edit_range" and drop the old column
    df = df.withColumn(bin_col, sf.when(sf.isnan(bin_col), len(labels)-1).otherwise(sf.col(bin_col)))

    # Define the when conditions and corresponding labels
    when_conditions = [sf.col(bin_col) == i for i in range(len(labels))]
    when_labels = [sf.lit(labels[i]) for i in range(len(labels))]

    for i in range(len(when_conditions)):
        df = df.withColumn(bin_col, sf.when(when_conditions[i], when_labels[i]).otherwise(sf.col(bin_col)))
        
    return df

def run_dp_count(df, keyset_df, epsilon, input_col, bin_col, private_output_col, nonprivate_output_col):
    # Create keyset
    ks = KeySet.from_dataframe(keyset_df)
    
    # cache df for performance
    df.cache()
    df.take(1)
    
    # Create session
    session = Session.from_dataframe(
        privacy_budget=PureDPBudget(epsilon),
        source_id="geoeditors_count",
        dataframe=df,
        protected_change=AddOneRow()
    )
    
    # Define query
    query = (
        QueryBuilder("geoeditors_count")
        .bin_column(input_col, edit_binspec, name=bin_col)
        .groupby(ks)
        .count()
    )

    # Run the query
    private = session.evaluate(query, PureDPBudget(epsilon=epsilon))
    
    # Clean-up outputs
    private = private.withColumnRenamed(f'count', private_output_col)
    private = private.withColumn(private_output_col, sf.round(sf.col(private_output_col)))
    
    # Bucketize the dataframe
    nonprivate_df = bucketize(df, input_col=input_col, bin_col=bin_col)
    nonprivate_df.cache()
    nonprivate_df.take(1)
    
    # Nonprivate aggregation
    nonprivate = nonprivate_df.groupby(['country_code', 'wiki_db', bin_col]).count()
    nonprivate = nonprivate.withColumnRenamed(f'count', nonprivate_output_col)

    return private, nonprivate

# strategy:
# - add bin column
# - split by bins into separate dfs
# - for each df:
#  - DP group by country-project and sum, with bin minimum as lower bound and bin maximum as upper bound
# - concatenate dfs back together
def run_dp_sum(df, keyset_df, epsilon, input_col, bin_col, private_output_col, nonprivate_output_col):
    # Bucketize the dataframe
    df = bucketize(df, input_col=input_col, bin_col=bin_col)
    df.cache()
    df.take(1)
    
    # calculate nonprivate agg
    nonprivate = df.groupby(['country_code', 'wiki_db', bin_col]).sum(input_col)
    nonprivate = nonprivate.withColumnRenamed(f'sum({input_col})', nonprivate_output_col)

    # Create meta session
    session = Session.from_dataframe(
        privacy_budget=PureDPBudget(float('inf')),
        source_id="geoeditors_sum",
        dataframe=df,
        protected_change=AddOneRow()
    )

    # For each df, define lo, hi, contrib_min, and contrib_max
    s = {}
    contrib_min_max = {}
    for i, l in enumerate(labels):
        lo = contrib_min = int(splits[i])
        if splits[i+1] == float('inf'):
            hi = str(splits[i+1])
            contrib_max = splits[i] + 1
        else:
            hi = contrib_max = int(splits[i+1])
            
        s[f'geoeditors_{lo}_{hi}'] = l
        contrib_min_max[f'geoeditors_{lo}_{hi}'] = [contrib_min, contrib_max]

    sessions = session.partition_and_create(
        "geoeditors_sum",
        privacy_budget=PureDPBudget(epsilon),
        column=bin_col,
        splits=s
    )
    
    private_dfs = []
    for i, (name, sess) in enumerate(sessions.items()):
        # create keyset
        filter_keyset_df = keyset_df.filter(keyset_df.edit_range == labels[i])
        ks = KeySet.from_dataframe(filter_keyset_df)
        # Define query as a groupby-sum
        query = (
            QueryBuilder(name)
            .groupby(ks)
            .sum(column=input_col, low=contrib_min_max[name][0], high=contrib_min_max[name][1])
        )
        
        # Run the query
        private = sess.evaluate(query, PureDPBudget(epsilon=epsilon))
        
        # Add the bucket back to the private output and append to output list
        private = private.withColumn(bin_col, sf.lit(labels[i]))
        private_dfs.append(private)
        sess.stop()

    # Union all private dfs together
    private = private_dfs[0]
    for df in private_dfs[1:]:
        private = private.union(df)
        
    private = private.withColumnRenamed(f'{input_col}_sum', private_output_col)
    private = private.withColumn(private_output_col, sf.round(sf.col(private_output_col)))

    return private, nonprivate

def write_loss_to_hive(spark, start, periods):
    loss = []
    for d in pd.date_range(start=start, periods=periods):
        loss.append(('wmf.editors_daily', float(initial_eps), float(0), None, d.strftime("%Y-%m-%d"), d.year, d.month, d.day))
        for i in range(3):
            loss.append(('wmf.editors_daily', float(subsequent_eps), float(0), None, d.strftime("%Y-%m-%d"), d.year, d.month, d.day))

    loss = spark.createDataFrame(loss, loss_schema)
#     loss.write.mode("append").insertInto("differential_privacy.global_privacy_register")

#------------ END HELPER FUNCTIONS ------------#

#------------ START MAIN FUNCTION ------------#
def main(df, keyset_df, time_frame, args, log):
    # run initial count
    log.info('running initial private count aggregation...')
    private_count, nonprivate_count = run_dp_count(
        df,
        keyset_df=keyset_df,
        epsilon=initial_eps,
        input_col=input_cols[0],
        bin_col=bin_col,
        private_output_col=private_output_cols[0],
        nonprivate_output_col=nonprivate_output_cols[0]
    )
    
    log.info(f'filtering initial private count aggregation to just ≥ {release_thresh}...')
    private_count = private_count.filter(private_count.private_count >= release_thresh)
    
    # get keyset for subsequent aggregations
    log.info('getting keyset for subsequent aggregations...')
    sum_keyset_df = (
        private_count.filter(private_count.private_count >= release_thresh)
        .select(join_keys)
    )
    
    # run private sum
    log.info('running subsequent aggregations...')
    log.info('sum...')
    private_sum, nonprivate_sum = run_dp_sum(
        df,
        keyset_df=sum_keyset_df,
        epsilon=subsequent_eps,
        input_col=input_cols[0],
        bin_col=bin_col,
        private_output_col=private_output_cols[1],
        nonprivate_output_col=nonprivate_output_cols[1]
    )
        
    # calculate errors
    log.info('calculating errors...')
    private_dfs = [private_count, private_sum]
    nonprivate_dfs = [nonprivate_count, nonprivate_sum]
    errs = []
#     for i, (private, nonprivate) in enumerate(zip(private_dfs, nonprivate_dfs)):
#         errs.append(calc_error_geoeditors(
#         private,
#         nonprivate,
#         noisy_col=private_output_cols[i],
#         exact_col=nonprivate_output_cols[i]
#     ))
    
    
    # join all private dfs together and fill negatives/nas with 0
    log.info('joining all dfs together...')
    private = (
        private_count.join(private_sum, on=join_keys, how='outer')
        .withColumn('private_sum', sf.when(sf.col('private_sum') < 0, 0).otherwise(sf.col('private_sum').cast('int')))
        .withColumn(time_frame, sf.lit(args.time_start))
    )
    
    # calculate total and pct inconsistent
    log.info('calculating inconsistencies...')
    total_inconsistent = (
        private.filter(
            ((private.private_count > 0) & (private.private_sum == 0))
        ).count()
    )
    pct_inconsistent = total_inconsistent / private.count()
    
    log.info('adding inconsistencies to error log...')
#     for err in errs:
#         err[time_frame] = args.time
#         err['total_inconsistent'] = total_inconsistent
#         err['pct_inconsistent'] = pct_inconsistent
        
    log.info('converting errors to spark df...')
#     errs = spark.createDataFrame(
#         [(err['name'],
#           err['released'],
#           err['med_rel_err'],
#           err['dropped'],
#           err['spurious'],
#           err['rel_err_fraction_0_5'],
#           err['rel_err_fraction_0_9'],
#           err['total_inconsistent'],
#           err['pct_inconsistent'],
#           err[time_frame]) for err in errs],
#         )
    
    log.info('done')
    return private#, errs
#------------ END MAIN FUNCTION ------------#

if __name__ == "__main__":
    # ===== SETUP =====
    spark = SparkSession.builder.getOrCreate()
    spark.sql("SET hive.exec.dynamic.partition=true")
    spark.sql("SET hive.exec.dynamic.partition.mode=nonstrict")

    log = spark.sparkContext._jvm.org.apache.log4j.LogManager.getLogger(__name__)
    
    month = datetime(2023, 7, 1) # TODO: make configurable with configargparse
    month_str = month.strftime("%Y-%m")
    month_args = GeoEditorsArgs(month_str, 'htriedman.geoeditors_debug')
    
    # create global keyset
    log.info('creating keyset and setting constants...')
    bin_df = spark.createDataFrame([(label,) for label in labels], ["edit_range"])
    keyset_df = spark.sql(keyset_query)
    keyset_df = keyset_df.crossJoin(bin_df)
    
    # ===== MONTHLY AGGREGATION =====
    # get raw data
    log.info('doing monthly query...')
    df = spark.sql(monthly_edit_query.format(month=month_str))
    df = df.dropna()
    
    log.info('doing monthly aggregation...')
    private = main(df=df, keyset_df=keyset_df, time_frame='month', args=month_args, log=log)
    
    log.info('writing monthly output to hive...')
    private.write.mode("append").insertInto(month_args.output_table)
    log.info('writing monthly error to hive...')
#     errs.write.mode("append").insertInto("differential_privacy.geoeditors_weekly_error")

    log.info('writing monthly loss to hive...')
#     write_loss_to_hive(spark, start=month_args.time, periods=pd.Period(month_args.time).days_in_month)

    log.info('cleaning up...')
    cleanup()
    spark.sparkContext.stop()
    spark.stop()