from datetime import date
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DoubleType
from pyspark.sql import DataFrameWriter

global_priv_register = 'differential_privacy.global_privacy_register'

def date_to_str(year, month, day):
    return date(year=year, month=month, day=day).strftime("%Y-%m-%d")

def write_to_global_privacy_budget(spark, table, epsilon, year, month, day):
    schema = StructType([       
        StructField('table', StringType(), False),
        StructField('epsilon', DoubleType(), False),
        StructField('delta', DoubleType(), True),
        StructField('rho', DoubleType(), True),
        StructField('date', StringType(), False),
        StructField('year', IntegerType(), False),
        StructField('month', IntegerType(), False),
        StructField('date', IntegerType(), False)
    ])

    df = spark.createDataFrame(
        data=[(table, float(epsilon), float(0), None, date_to_str(year, month, day), year, month, day)],
        schema=schema
    )
    DataFrameWriter(df).insertInto(global_priv_register)
