from pyspark.sql import functions as sf
import math

bin_col = 'edit_range'
join_keys = ['country_code', 'wiki_db', bin_col]

def calc_rel_err(df, noisy_col, exact_col):
    return df.withColumn('rel_err', sf.abs(sf.col(exact_col) - sf.col(noisy_col)) / sf.col(exact_col))

def calc_med_rel_err(df, release_thresh, noisy_col, exact_col):
    published_non_null = df.filter((df[noisy_col] >= release_thresh) & (df[exact_col] > 0))
    return published_non_null.approxQuantile('rel_err', [0.5], 0.001)[0]

def calc_dropped(df, release_thresh, noisy_col, exact_col):
    non_published = df.filter(
        (df[noisy_col] > 0) &
        (df[noisy_col] < release_thresh) &
        (df[exact_col] >= release_thresh)
    )
    should_be_published = df.filter(df[exact_col] >= release_thresh)
    return non_published.count() / should_be_published.count()

def calc_spurious(df, release_thresh, noisy_col, exact_col):
    published = df.filter(df[noisy_col] >= release_thresh)
    return published.filter(published[exact_col] == 0).count() / published.count()

def calc_released(df, release_thresh, noisy_col):
    published = df.filter(df[noisy_col] >= release_thresh)
    return published.count()

def calc_high_relative_error_fraction(df, release_thresh, noisy_col, exact_col, quantile):
    published_non_null = df.filter((df[noisy_col] >= release_thresh) & (df[exact_col] > 0))
    return published_non_null.filter(published_non_null.rel_err > quantile).count() / published_non_null.count()

def calc_error_geoeditors(private, nonprivate, noisy_col, exact_col, release_thresh=1):
    joined = (
        private.join(nonprivate, on=join_keys, how='outer')
        .fillna(0)
        .withColumn(noisy_col, sf.when(sf.col(noisy_col) < 0, 0).otherwise(sf.col(noisy_col)))
    )

    joined = calc_rel_err(joined, noisy_col=noisy_col, exact_col=exact_col)
    joined.cache()
    joined.take(1)
    
    err = {}
    err['name'] = noisy_col
    err['released'] = calc_released(joined, release_thresh=release_thresh, noisy_col=noisy_col)
    err['med_rel_err'] = calc_med_rel_err(joined, release_thresh=release_thresh, noisy_col=noisy_col, exact_col=exact_col)
    err['dropped'] = calc_dropped(joined, release_thresh=release_thresh, noisy_col=noisy_col, exact_col=exact_col)
    err['spurious'] = calc_spurious(joined, release_thresh=release_thresh, noisy_col=noisy_col, exact_col=exact_col)
    err['rel_err_fraction_0_5'] = calc_high_relative_error_fraction(joined, release_thresh=release_thresh, noisy_col=noisy_col, exact_col=exact_col, quantile=0.5)
    err['rel_err_fraction_0_9'] = calc_high_relative_error_fraction(joined, release_thresh=release_thresh, noisy_col=noisy_col, exact_col=exact_col, quantile=0.9)
    return err