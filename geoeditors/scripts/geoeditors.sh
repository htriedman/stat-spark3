#!/bin/bash

# runs the geoeditors.py script to create DP counts of country-project-editor/edit tuples
source /opt/conda-analytics/etc/profile.d/conda.sh

python_version=3.9
export venv=tumult
conda activate ${venv}

# ## Spark is installed in the tumult conda environment.
export SPARK_HOME=${HOME}/.conda/envs/${venv}/lib/python${python_version}/site-packages/pyspark
export SPARK_CONF_DIR=/etc/spark3/conf/
export HADOOP_CONF_DIR=/etc/hadoop/conf/
export PYSPARK_SUBMIT_ARGS="pyspark-shell"
export HIVE_CONF_DIR=/etc/spark3/conf/
export PYSPARK_PYTHON=./venv/bin/python

$SPARK_HOME/bin/spark-submit \
    --archives venv-conda.tar.gz#venv \
    --master yarn \
    --files /etc/spark3/conf/hive-site.xml \
    --deploy-mode cluster \
    --driver-memory 8g \
    --driver-cores 2 \
    --executor-memory 8g \
    --executor-cores 4 \
    --conf 'spark.sql.warehouse.dir=/tmp' \
    --conf 'spark.dynamicAllocation.maxExecutors=80' \
    --conf 'spark.executor.memoryOverhead=2g' \
    --conf 'spark.driver.memoryOverhead=2g' \
    geoeditors/jobs/geoeditors.py

conda deactivate
