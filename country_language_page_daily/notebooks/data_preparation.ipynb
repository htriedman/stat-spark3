{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b2dc1ac8",
   "metadata": {},
   "source": [
    "# Summary\n",
    "\n",
    "This notebook contains a set of pyspark query to generate a `KeySet` that we'll use for differentially private analytics.\n",
    "It acts as a test bed to understand bounds of our data processing capabilities, and to experiment with spark tunables and\n",
    "data modelling optimizations.\n",
    "\n",
    "The largest KeySet I was able to build within reasonable time (<120 minutes) is based on 30 days (1-30 March 2022)\n",
    "of `pageview_actor` data. This is roughly 1/3 of the entire data.\n",
    "\n",
    "The resulting dataset is approx $3 * 10^{10}$ records.\n",
    "It is available for offline analysis at `/user/gmodena/pa_key_set`.\n",
    "\n",
    "## Suggestions\n",
    "The best solution, if feasible, would be to reduce the input size. \n",
    "For instance by filtering out actor signatures with less than $k$\n",
    "views.\n",
    "\n",
    "In general we should avoid recomputing data if not necessary. For experiments, we could:\n",
    "1. Create a keyset periodically, store it to HDFS, and load it when necessary.\n",
    "2. `.cache()` large datasets used more than once across queries.\n",
    "\n",
    "We can still expect some performance penalty, since we run spark with shuffle service and auto scaling\n",
    "disabled. One tunable to control is the number of dataframe partitions \n",
    "(`spark.sql.shuffle.partitions`). As a baseline, for the larger datasets, \n",
    "we should set it to `num_cores * num_executors`.\n",
    "\n",
    "The bulk of this query is a cross join between article and country data. More details in the cells below.\n",
    "\n",
    "## Next steps\n",
    "\n",
    "- try to run the queries with spark2, and produce a dataset we can feed into tumult's lib.\n",
    "- re-evaluate executor vs core tradeoffs.\n",
    "\n",
    "## Things that did not work\n",
    "- dataframe broadcasting can result in high GC pauses during crossJoins, this did not seem to be the case for us.\n",
    " (we can turn off broadcasting by setting `spark.sql.autoBroadcastJoinThreshold=0`).\n",
    "- variouns combinations of num executor and num cores"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69f4b010",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fd8450c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import requests\n",
    "import math\n",
    "import random\n",
    "from datetime import timedelta, date\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de46b1e4",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "os.environ.get(\"SPARK_HOME\")\n",
    "\n",
    "# Assumes $HOME/pyspark_dp_beta/venv.tar.gz exists\n",
    "venv = os.path.join(os.environ['HOME'], 'stat-spark3/venv-conda.tar.gz#venv')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "505c762c",
   "metadata": {},
   "source": [
    "In general we would expect better HDFS by trading an increased number of executors for a decreased number of cores/executor. Empirically, however, bumping `spark.executor.cores` to `12` did lead to faster runtimes for large table scans in `pv_query`. Here I bumped `spark.executor.memory` to keep the recommended 2g/core ratio.\n",
    "\n",
    "This is ad-hoc tuning for running `pv_query`, for the general case we should look at allocating more executors with less cores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c018ee7a",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from pyspark.sql import SparkSession\n",
    "os.environ['PYSPARK_PYTHON'] = './venv/bin/python'\n",
    "spark = (\n",
    "    SparkSession.builder.master('yarn')\n",
    "        .config('spark.yarn.dist.archives', venv)\n",
    "        .config('spark.sql.warehouse.dir', '/tmp')\n",
    "        .config('spark.executor.instances', '24')\n",
    "        .config('spark.executor.memory', '24g')\n",
    "        .config('spark.executor.cores', '12')\n",
    "        .getOrCreate()\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94dc7bc1",
   "metadata": {},
   "source": [
    "## KeySet preparation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e85cebb",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyspark.sql import functions as sf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8b937b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "country_query = \"\"\"\n",
    "SELECT DISTINCT\n",
    "  country_code as country\n",
    "FROM\n",
    "  wmf.geoeditors_public_monthly\n",
    "WHERE country_code != '--'\n",
    "\"\"\"\n",
    "    \n",
    "pv_query = \"\"\"\n",
    "SELECT DISTINCT\n",
    "  pageview_info['page_title'] as page_title,\n",
    "  page_id,\n",
    "  pageview_info['project'] as project\n",
    "  , geocoded_data['country'] as country\n",
    "  , actor_signature\n",
    "FROM wmf.pageview_actor\n",
    "WHERE\n",
    "  is_pageview \n",
    "  AND namespace_id = 0\n",
    "  AND year = 2022\n",
    "  AND month = 3\n",
    "  AND pageview_info['page_title'] IS NOT NULL\n",
    "  AND page_id IS NOT NULL\n",
    "  AND pageview_info['project'] IS NOT NULL\n",
    "  AND geocoded_data['country'] IS NOT NULL\n",
    "  AND actor_signature IS NOT NULL\n",
    "\"\"\"\n",
    "df = spark.sql(pv_query)\n",
    "df = (\n",
    "    df.filter(df.page_title != 'NaN')\n",
    "    .filter(df.page_title != 'Nan')\n",
    "    .filter(df.page_title != 'nan')\n",
    "    .filter(df.page_title != 'NAN')\n",
    ")\n",
    "article_df = df.select(\"project\", \"page_id\").distinct()\n",
    "country_df = spark.sql(country_query)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb20b7ba",
   "metadata": {},
   "source": [
    "Cache `country_df` and materialize it before the crossJoin starts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6196f7ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "country_df.cache()\n",
    "country_df.show(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41e85eb0",
   "metadata": {},
   "outputs": [],
   "source": [
    "key_df = country_df.crossJoin(article_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6da2cfb",
   "metadata": {},
   "source": [
    "`explain()` will produce the physical plan for this query."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd1f2aa8",
   "metadata": {},
   "outputs": [],
   "source": [
    "key_df.explain()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44444717",
   "metadata": {},
   "source": [
    "Repartition data to evenly distribute across resources. `num_executors * num_cores` is a starting point, we shuold futher tune based on throghput and eventual skewness."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8af65f9c",
   "metadata": {},
   "outputs": [],
   "source": [
    "num_cores = 12\n",
    "num_executors = 24\n",
    "key_df.repartition(num_executors * num_cores)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c12f7daa",
   "metadata": {},
   "source": [
    "I cache `key_df` because I'll perform two operations on it later on (`count()` and `write.parquet()`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1acb80b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "key_df.cache()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "820e0def",
   "metadata": {},
   "source": [
    "The following action will materialize `key_df`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b6b7a65",
   "metadata": {},
   "outputs": [],
   "source": [
    "key_df.count()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e735c740",
   "metadata": {},
   "source": [
    "We should get a count in the order of $3*10^{10}$ records."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cab50bb9",
   "metadata": {},
   "source": [
    "Save the keyset for further use. For instance, in analytics notebooks. The serialisation will result in `num_executors * num_cores`. We could reduce the number of files\n",
    "(future tunable) by calling `coalesce(n_partition)` before the write."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0773dcfa",
   "metadata": {},
   "outputs": [],
   "source": [
    "key_df.write.parquet('pa_key_set')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b61c758",
   "metadata": {},
   "source": [
    "Data is saved under my home dir (e.g `/home/gmodena/pa_key_set`). For other colleagues (who signed the NDA) to access it, we can set group ownerhips with:\n",
    "```\n",
    "hadoop fs -chown -R gmodena:analytics-privatedata-users /user/gmodena/pa_key_set\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
