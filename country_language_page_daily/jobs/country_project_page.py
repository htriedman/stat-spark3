# system packages
import os
import configargparse
from datetime import datetime

# pyspark
from pyspark.sql import SparkSession
from pyspark.sql import functions as sf

# tumult analytics
from tmlt.analytics.privacy_budget import PureDPBudget
from tmlt.analytics.query_builder import QueryBuilder
from tmlt.analytics.session import Session
from tmlt.analytics.keyset import KeySet

# tumult core
from tmlt.core.domains.spark_domains import SparkDataFrameDomain
from tmlt.core.transformations.spark_transformations.join import HashTopKTruncation
from tmlt.core.utils.cleanup import cleanup

# all pageviews for a day from pageview_actor
pv_query = """
SELECT
  page_id,
  pageview_info['project'] as project,
  geocoded_data['country'] as country,
  actor_signature
FROM wmf.pageview_actor
WHERE
  is_pageview 
  AND namespace_id = 0
  AND year = {year}
  AND month = {month}
  AND day = {day}
"""

# select all unique pages from pageview_actor with more than `count` views
filter_query = """
SELECT
  pageview_info['project'] AS project,
  page_id
FROM
  wmf.pageview_actor
WHERE
  is_pageview 
  AND COALESCE(pageview_info['project'], '') != ''
  AND year = {year}
  AND month = {month}
  AND day = {day}
GROUP BY 1, 2
HAVING count(*) >= {pv_thresh}
"""

# all countries we are releasing data for
country_query = """
SELECT DISTINCT
  country_name as country
FROM
  wmf.geoeditors_public_monthly
WHERE country_code != '--'
"""

def make_argparse():
    parser = configargparse.ArgumentParser(
        description="Compute one day's worth of differentially private country-language-page-count tuples."
    )
    parser.add(
        "-c", "--config", required=False, is_config_file=True, help="config file path"
    )
    parser.add_argument(
        "--year",
        metavar="year",
        type=str,
        help="Year YYYY",
        default=datetime.today().strftime("%Y"),
    )
    parser.add_argument(
        "--month",
        metavar="month",
        type=str,
        help="Month",
        default=datetime.today().strftime("%m"),
    )
    parser.add_argument(
        "--day",
        metavar="day",
        type=str,
        help="Day",
        default=datetime.today().strftime("%d"),
    )
    parser.add_argument(
        "--epsilon",
        metavar="epsilon",
        type=int,
        help="Day",
        default=1,
    )
    parser.add_argument(
        "--pv_thresh",
        metavar="pv_thresh",
        type=int,
        help="Pageview threshold",
        default=100,
    )
    parser.add_argument(
        "--contrib_thresh",
        metavar="contrib_thresh",
        type=int,
        help="Contribution threshold",
        default=10,
    )
    return parser

def main(args):
    os.environ.get("SPARK_HOME")

    # Assumes $HOME/pyspark_dp_beta/venv.tar.gz exists
    venv = os.path.join(os.environ['HOME'], 'stat-spark3/venv-conda.tar.gz#venv')

    os.environ['PYSPARK_PYTHON'] = './venv/bin/python'
    spark = (
        SparkSession.builder.master('yarn')
            .config('spark.yarn.dist.archives', venv)
            .config('spark.sql.warehouse.dir', '/tmp')
            .config('spark.executor.instances', '24')
            .config('spark.executor.memory', '24g')
            .config('spark.executor.cores', '12')
            .getOrCreate()
    )
    # select all pageviews from a day and filter out both pages with a title of "nan" and actual NaNs
    df = spark.sql(pv_query.format(year=args.year, month=args.month, day=args.day))
    df = df.dropna()

    # select all countries we're releasing data for
    country_df = spark.sql(country_query)
    country_df.cache()
    country_df.collect()

    # create Tumult df domain
    domain = SparkDataFrameDomain.from_spark_schema(df.schema)
    
    # create article 
    article_df = spark.sql(filter_query.format(year=args.year, month=args.month, day=args.day, pv_thresh=args.pv_thresh))
    article_df.cache()
    article_df.collect()

    # cross join countries and articles to get keyspace and cache
    key_df = country_df.crossJoin(article_df)
    key_df = key_df.dropna()
    key_df.cache()
    ks = KeySet.from_dataframe(key_df)
    
    # truncate contributions by actor signature to contrib_thresh at maximum
    truncation = HashTopKTruncation(
        domain=domain,
        keys=["actor_signature"],
        threshold=args.contrib_thresh,
    )
    df_limited = truncation(df.select("*").distinct())
    df_limited.cache()
    
    session = Session.from_dataframe(
        privacy_budget=PureDPBudget(float('inf')),
        source_id="pageview_actor",
        dataframe=df_limited,
        stability=args.contrib_thresh
    )
    
    # run actual groupby-count query
    qb = QueryBuilder("pageview_actor")
    query = qb.groupby(ks).count(name='groupby_count')
    private = session.evaluate(query, PureDPBudget(epsilon=args.epsilon))
    
    # save output
#     private.write.parquet(f"dp_eps_{str(args.epsilon).replace('.', '_')}_pv_thresh_{args.pv_thresh}_contrib_thresh_{args.contrib_thresh}_{args.year}_{args.month}_{args.day}")
    private.createOrReplaceTempView('temp')
    spark.sql(f"CREATE TABLE htriedman.dp_eps_{str(args.epsilon).replace('.', '_')}_pv_thresh_{args.pv_thresh}_contrib_thresh_{args.contrib_thresh}_{args.year}_{args.month}_{args.day} AS SELECT * FROM temp")
#     spark.sql(f"CREATE TABLE htriedman.dp_test_script_2022_5_11 AS SELECT * FROM temp")

    
    cleanup()
    spark.sparkContext.stop()
    spark.stop()
    
if __name__ == "__main__":
    parser = make_argparse()
    print("made parser")
    args = parser.parse_args()
    print(f"Creating pageview histogram for {args.year}-{args.month}-{args.day} (YYYY-MM-DD)")
    print(f"Epsilon = {args.epsilon}\nPage inclusion threshold = {args.pv_thresh}\nContribution threshold = {args.contrib_thresh}\n")
    main(args)


