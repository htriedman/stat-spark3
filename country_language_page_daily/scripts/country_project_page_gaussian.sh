#!/bin/bash

# runs the country_project_page.py script to create DP counts of country-project-page tuples
# Application specific config is available at ../conf/country_project_page.conf
# Run ../jobs/country_project_page_gaussian.py --help for a summary of command line options that
# override country_project_page.conf

python_version=3.7
export venv=tumult
source /usr/lib/anaconda-wmf/bin/conda-activate-stacked ${venv}
# spark configs
## Spark is installed in the tumult conda environment.
export SPARK_HOME=${HOME}/.conda/envs/${venv}/lib/python${python_version}/site-packages/pyspark
## Configs under SPARK_CONF_DIR will override defaults.
## Since SPARK_HOME points to a directory in PYTHONPATH, we keep relevant
## config files in a user owned path.
export SPARK_CONF_DIR=$(echo ${HOME})/stat-spark3/conf/
export HADOOP_CONF_DIR=/etc/hadoop/conf/
export PYSPARK_SUBMIT_ARGS="pyspark-shell"
export HIVE_CONF_DIR=/etc/spark2/conf/
export PYSPARK_PYTHON=./venv/bin/python

$SPARK_HOME/bin/spark-submit \
    --archives venv-conda.tar.gz#venv \
    --deploy-mode cluster \
    --files /etc/spark2/conf/hive-site.xml \
    country_language_page_daily/jobs/country_project_page_gaussian.py

source /usr/lib/anaconda-wmf/bin/conda-deactivate-stacked
